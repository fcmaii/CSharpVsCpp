#include "stdafx.h"
#include "CppFunction.h"


CppFunction::CppFunction()
{
}


CppFunction::~CppFunction()
{
}


class CCalc
{
public:
	CCalc(int a, int b)
	{
		m_a = a;
		m_b = b;
	}

	int Calc()
	{
		if (m_a % 2 == 0) {
			return m_a + m_b;
		}
		if (m_b % 2 == 0) {
			return m_a - m_b;
		}
		return m_b - m_a;
	}

private:
	int m_a;
	int m_b;
};

int CppFunction::TestFunc(int a, int b)
{
	CCalc calc(a, b);
	return calc.Calc();
}
